package it.disco.unimib.labeller.test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import it.disco.unimib.labeller.index.IndexFields;

import org.junit.Test;

public class IndexFieldsTest {

	@Test
	public void ifKbIsYagoShouldConsiderLabels() throws Exception {
		String property = new IndexFields().predicateField();
		
		assertThat(property, equalTo("label"));
	}
}
