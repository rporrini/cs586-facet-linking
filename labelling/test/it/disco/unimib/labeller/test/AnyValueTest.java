package it.disco.unimib.labeller.test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import it.disco.unimib.labeller.index.AnyValue;
import it.disco.unimib.labeller.index.IndexFields;

import org.apache.lucene.search.BooleanQuery;
import org.junit.Test;

public class AnyValueTest {

	@Test
	public void shouldParseAlsoQueriesWithORs() throws Exception {
		
		BooleanQuery query = new AnyValue().createQuery("portland, OR", "label", new IndexFields().analyzer());
		
		assertThat(query, is(notNullValue()));
	}
}
