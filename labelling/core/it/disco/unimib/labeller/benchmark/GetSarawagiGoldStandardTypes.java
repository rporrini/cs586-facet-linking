package it.disco.unimib.labeller.benchmark;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class GetSarawagiGoldStandardTypes {
	public static void main(String[] args) throws Exception {
		String targetDirectory = args[0];
		String goldStandardPath = args[1];
		System.out.println(goldStandardPath);
		Element anno = null;

		List<Integer> colIndexes = new ArrayList<Integer>();
		List<String> colTypes = new ArrayList<String>();

		DocumentBuilderFactory documentFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder builder = documentFactory.newDocumentBuilder();
		Collection<File> relationsFiles = getRelationsFiles(goldStandardPath);
		int relationsWithContext = 0;
		for (File relationFiles : relationsFiles) {
			NodeList elements = builder.parse(relationFiles)
					.getElementsByTagName("colAnnos");
			// loop on the colAnnos of this file
			for (int i = 0; i < elements.getLength(); i++) {
				Element e = (Element) elements.item(i);
				int columnId = Integer.parseInt(e.getAttribute("col"));
				
				NodeList childs = e.getChildNodes();
				for(int j = 0; j< childs.getLength(); j++){
					 if(childs.item(j).getNodeType() == Node.ELEMENT_NODE)
						 anno = (Element)childs.item(j);
				}
				
				if(anno.getAttribute("name").equalsIgnoreCase("NULL"))
					break;
				colTypes.add(filterNameType(anno.getAttribute("name")));
				colIndexes.add(columnId);

				// name of the predicate
				String relation = "";
				if (!relation.equalsIgnoreCase("NULL")) {
					Document table = builder.parse(tableFrom(relationFiles));
					String contextObtainedFromColumn = contextOf(
							builder.parse(relationFiles), columnId);
					relationsWithContext++;
					saveGroup(targetDirectory, relationsWithContext,
								relationFiles, relation,
								groupContent(table, columnId),
								contextObtainedFromColumn);
				}
			}
		}
		System.out.println("Finish");
	}

	private static String filterNameType(String s) {
		String class_ = new String();
		class_ = s.split("_")[1];
		return class_;
	}

	private static void saveGroup(String targetDirectory,
			int relationsWithContext, File relationFiles, String relation,
			ArrayList<String> groupContent, String context) throws Exception {
		File group = new File(targetDirectory + "/" + relationsWithContext
				+ "_sarawagi_" + context.trim() + "_" + relation);
		String sourceFile = tableFrom(relationFiles).getPath().replace("../",
				"");
		groupContent.add(0, "#" + sourceFile);
		FileUtils.writeLines(group, groupContent);
	}

	private static String contextOf(Document table, int columnId) {
		String context = "";
		NodeList columns = table.getElementsByTagName("colAnnos");
		for (int i = 0; i < columns.getLength(); i++) {
			Element columnElement = (Element) columns.item(i);
			int column = Integer.parseInt(columnElement.getAttribute("col"));
			if (column == columnId) {
				NodeList contexts = columnElement.getElementsByTagName("anno");
				for (int j = 0; j < contexts.getLength(); j++) {
					Element contextElement = (Element) contexts.item(j);
					context += " " + contextElement.getAttribute("name");
				}
			}
		}
		return context.replace("_", "-");
	}

	/**
	 * 
	 * @param tablesDocument
	 * @param cell
	 *            the index of the column
	 * @return
	 */
	private static ArrayList<String> groupContent(Document tablesDocument,
			int cell) {
		ArrayList<String> groupContent = new ArrayList<String>();
		NodeList rows = tablesDocument.getElementsByTagName("row");
		for (int i = 0; i < rows.getLength(); i++) {
			Element row = (Element) rows.item(i);
			NodeList rowcells = row.getElementsByTagName("cell");
			groupContent.add(valueOf(cell, rowcells));
		}
		return groupContent;
	}

	private static String valueOf(int cellNumber, NodeList cells) {
		Element cell = (Element) cells.item(cellNumber);
		String cellValue = "";
		try {
			cellValue = cell.getElementsByTagName("text").item(0)
					.getFirstChild().getNodeValue();
		} catch (Exception e) {
		}
		return cellValue;
	}

	private static Collection<File> getRelationsFiles(String goldStandardPath)
			throws IOException {
		return FileUtils.listFiles(new File(goldStandardPath
				+ "workspace/WWT_GroundTruth/annotation"),
				new String[] { "xml" }, true);
	}

	private static File tableFrom(File labelFile) {
		return new File(labelFile.getPath().replace(
				"workspace/WWT_GroundTruth/annotation", "tablesForAnnotation"));
	}

}
