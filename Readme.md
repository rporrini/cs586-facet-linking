## Requisiti di sistema

* linux
* git
* eclipse

## Per iniziare a sviluppare

```
git clone https://{USERNAME}:{PASSWORD}@bitbucket.org/rporrini/cs586-facet-linking.git
cd cs586-facet-linking
./build-and-test.sh
```

La procedura di build installa l'insieme dei tool necessari (compreso java) e prevede anche lo scaricamento dei dataset per la sperimentazione. Per questo motivo la prima volta che viene eseguita dura un pò (nell'ordine dei 10 minuti). In condizioni normali, 30 secondi.

## Per far girare i test all'interno di un IDE

* selezionare la classe di ```TestSuite.java```
* eseguirla con ```junit```

## Creazione degli indici
Dalla root del repository:

```
./build-index-for-yago.sh
```

Per creare gli indici di Yago. Tempo stimato per l'indicizzazione: circa __9__ ore

## Creazione del Gold Standard di Limaye (Yago)
Dalla root del repository:

```
./generate-sarawagi-gold-standard.sh
passo di pulizia a mano :(
./generate-sarawagi-qrels.sh
```

## Run di un algoritmo
Lo script stampa su standard output i risultati dell'algoritmo in formato Trec Eval. Dalla root del repository:

```
./run-algorithm.sh
```

## Gold Standard

Il gold standard utilizzato è nella cartella ```evaluation/tools/annotationData/```

## Calcolo delle metriche via trec_eval
Dalla root del repository:

```
./evaluate-results.sh KNOWLEDGE-BASE GOLD-STANDARD RESULTS-FILE
```

Parametri:

* KNOWLEDGE-BASE: yago1, yago1-simple
* GOLD-STANDARD: file formato trec del gold standard (si trovano nella cartella ```evaluation/results/```)
* RESULTS-FILE: file formato trec dei risultati degli algoritmi (si trovano nelle cartelle ```evaluation/results/KNOWLEDGE-BASE-results```)

## Calcolo delle metriche di tutti gli algoritmi via trec_eval
Dalla root del repository:

```
./evaluate-all-results.sh KNOWLEDGE-BASE
```

Lo script prende tutti i risultati prodotti da ```./run-algorithm.sh``` e li confronta con il gold-standard generando un file ```all-results.csv``` contenente il confronto
Parametri:

* KNOWLEDGE-BASE: yago1, yago1-simple