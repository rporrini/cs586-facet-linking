#!/bin/bash

set -e
relative_path=`dirname $0`
root=`cd $relative_path;pwd`
cd $root

sh key-value-index-osx.sh yago1-types yago1/types "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
sh key-value-index-osx.sh yago1-labels yago1/labels "http://www.w3.org/2000/01/rdf-schema#label"
sh predicate-value-index-osx.sh yago1-properties yago1/properties yago1/types yago1/labels 2