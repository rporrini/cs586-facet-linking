#!/usr/bin/python
#this scripts replace the special characters "&apos", "&amp;amp;" and "&quot;" with ',& and "
import os, sys

# Open a file
path = "/Users/giorgioconte/Documents/Giorgio/POLIMI/5-Anno/Fall_semester@UIC/CS-586-Data-Web_Semantics/Project/Source/cs586-facet-linking/evaluation/gold-standard-types"
dirs = os.listdir( path )
newPath = "/Users/giorgioconte/Documents/Giorgio/POLIMI/5-Anno/Fall_semester@UIC/CS-586-Data-Web_Semantics/Project/Source/cs586-facet-linking/evaluation/gold-standard-types-cleaned"

# This would print all the files and directories
for file in dirs:
   f = open(path+"/"+file, "rb")
   newFile = open(newPath + "/" +file, "wb")
   for line in f:
   		if line not in ['\n', '\r\n']:
   			line1 = line.replace("&apos;", "'")
   			line2 = line1.replace("&amp;amp;","&")
   			line3 = line2.replace("&quot;","\"")
   			newFile.write(line3)

